﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Threading.Tasks;

namespace Dashboards.DAL
{
    public class CommonDAL
    {
        public static void executeProcedure(string StoredProcedureName, out DataTable dtemp, [Optional] string[,] aryParameters)
        {
            using (SqlConnection con = new SqlConnection("Server=localhost;Database=schoolnme;User Id=saadmin;Password=Makhija@1;"))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    con.Open();
                    cmd.CommandType = CommandType.StoredProcedure;
                    SqlDataAdapter adp = new SqlDataAdapter();
                    DataTable dt = new DataTable();
                    cmd.CommandText = StoredProcedureName;
                    cmd.CommandTimeout = 300;
                    try
                    {
                        for (int i = 0; i < aryParameters.GetLength(0); i++)
                        {

                            cmd.Parameters.Add(new SqlParameter(aryParameters[i, 0], aryParameters[i, 1]));
                        }
                    }
                    catch (Exception ex)
                    {

                    }

                    cmd.Connection = con;
                    adp.SelectCommand = cmd;
                    adp.Fill(dt);
                    con.Close();
                    dtemp = dt;
                }
            }
        }

        public static DataTable ToDataTable<T>(List<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);

            //Get all the properties
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                //Defining type of data column gives proper data table 
                var type = (prop.PropertyType.IsGenericType && prop.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>) ? Nullable.GetUnderlyingType(prop.PropertyType) : prop.PropertyType);
                //Setting column names as Property names
                dataTable.Columns.Add(prop.Name, type);
            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {
                    //inserting property values to datatable rows
                    values[i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }
            //put a breakpoint here and check datatable
            return dataTable;
        }

    }
}
